#include <iostream>
#include <opencv2/opencv.hpp>
#include "FACE.h"

using namespace std;
using namespace cv;

int main()
{
	// image file path
	//string filePath = "C:/Users/User/desktop/New_Dataset/02_Orignal.jpg";
	int numOfImages = 99;
	string inPathPrefix = "C:/Users/user/Desktop/New_Dataset_3/";
	string inPathSuffix = "_Orignal.jpg";
	string outPathPrefix = "C:/Users/user/Desktop/New_Dataset_3/Results/FACE_";
	string outPathSuffix = ".jpg";
	for (int imageNumber = 1; imageNumber <= numOfImages; ++imageNumber)
	{
		string imageNoStr;
		if (imageNumber < 10) {
			imageNoStr = "0" + std::to_string(imageNumber);
		}
		else {
			imageNoStr = std::to_string(imageNumber);
		}
		string filePath = inPathPrefix + imageNoStr + inPathSuffix;
		string savePath = outPathPrefix + imageNoStr + outPathSuffix;

		Mat image = imread(filePath);
		int a = 0;
		// last input for iteration 
		{
			Mat out = FACE(image, a, 3, 3, 1, true, 15, 0, 1000, 15, 3);
			imwrite(savePath, out);
		}
		// Sencond input for value of alpha
		// double alpha = 1.0;
		// Mat imgOut = FACE_alpha(image, alpha, 3, 3, 1, true, 15, 0);
		// Function for entropy calculation
		// double entropy = entropy_cal(out);
		// cout << "Entropy = " << entropy << endl; // print the result
		// Save image
		//string savePath = "C:/Users/User/desktop/New_Dataset/Results/02_FACE.jpg";
		// imwrite(savePath, out);
	}
	return 0;
}