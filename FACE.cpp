#include "FACE.h"
#include <deque>


using namespace Eigen;
using namespace std;
using namespace cv;

void Loadimg(const Mat &Src, MatrixXd &L, MatrixXd &A, MatrixXd &B) {
    MatrixXd l, a, b;
    Mat Lab;
    vector<Mat> Lab1ch;
    if (!Src.empty()) {
        cvtColor(Src, Lab, COLOR_BGR2Lab);
        Mat Lab3ch = Lab.reshape(0, 1);
        split(Lab3ch, Lab1ch);
        cv2eigen(Lab1ch[0].t(), l);
        cv2eigen(Lab1ch[1].t(), a);
        cv2eigen(Lab1ch[2].t(), b);
        L = l.array();
        A = a.array();
        B = b.array();
//		L = l.array() / 255;
//		A = a.array() / 256;
//		B = b.array() / 256;
    } else {
        std::cout << "Error : No Image" << endl;
        cin.ignore();
        exit(-1);
    }
}

VectorXd LS(const MatrixXd &L, const MatrixXd &A, const MatrixXd &B) {
    MatrixXd one = MatrixXd::Ones(L.rows(), 1);
    MatrixXd MatA(L.rows(), 3);
    MatA << L, A, one;
    VectorXd Ans = MatA.jacobiSvd(ComputeThinU | ComputeThinV).solve(B);
    return Ans;
}

Matrix3d Rotation(Vector3d &VecA) {
    Matrix3d R, W_skew;
    Matrix3d Eye = Matrix3d::Identity();
    Vector3d VecZ(0, 0, 1);
    Vector3d W;
    W = VecZ.cross(VecA);
    W = W / W.norm();
    W_skew << 0, -W(2), W(1),
            W(2), 0, -W(0),
            -W(1), W(0), 0;
    VectorXd angle = VecZ.transpose() * VecA / VecZ.norm() / VecA.norm();
    double tht = acos(angle(0));
    R = Eye + W_skew * sin(tht) + W_skew * W_skew * (1 - cos(tht));
    return R;
}

MatrixXd Project(const MatrixXd &L, const MatrixXd &A, const MatrixXd &B, const Matrix3d &R) {
    MatrixXd Points(L.rows(), 3);
    MatrixXd zero = MatrixXd::Zero(L.rows(), 1);
    Points << L, A, B;
    MatrixXd NewP = Points * R;
    MatrixXd P2D(L.rows(), 2);
    P2D << NewP.col(0), NewP.col(1);
    return P2D;
}

MatrixXd Cutgrids(const MatrixXd input, MatrixXd &GridIndex, const int &Num) {
    MatrixXd Grids = MatrixXd::Zero(Num, Num);
    GridIndex.resize(input.rows(), 2);
    VectorXd MaxVal = input.colwise().maxCoeff();
    VectorXd MinVal = input.colwise().minCoeff();
    double PadX = (MaxVal(0) - MinVal(0)) / Num;
    double PadY = (MaxVal(1) - MinVal(1)) / Num;

    for (int i = 0; i < input.rows(); i++) {
        int X = max(int(ceil(((input(i, 0) - MinVal(0)) / PadX))) - 1, 0);
        int Y = max(int(ceil(((input(i, 1) - MinVal(1)) / PadY))) - 1, 0);
        Grids(X, Y) += 1;
        GridIndex.row(i) << X, Y;
    }
    return Grids;
}

MatrixXd
ClusterCenter(const MatrixXd &Grid, const MatrixXd &Grididx, const MatrixXd &L, const MatrixXd &A, const MatrixXd &B,
              const int &Num) {
    MatrixXd CPoint(Num, 3);
    Mat GMat, GSort, Gidx;
    eigen2cv(Grid, GMat);
    Mat Gvec = GMat.reshape(1, 1).t();
    sortIdx(Gvec, GSort, CV_SORT_EVERY_COLUMN + CV_SORT_DESCENDING);
    double Gsize = Grid.rows();
    if (Num <= countNonZero(Gvec)) {
        for (int i = 0; i < Num; i++) {
            int k = 0;
            MatrixXd P2Didx(int(Gvec.at<double>(GSort.at<int>(i))), 3);
            int X = int(GSort.at<int>(i) / Gsize);
            int Y = int(GSort.at<int>(i) - (X * (Gsize)));
            for (int j = 0; j < Grididx.rows(); j++) {
                VectorXd eigXY = Grididx.row(j);
                if (eigXY(0) == X && eigXY(1) == Y) {
                    P2Didx.row(k) << L.row(j), A.row(j), B.row(j);
                    k++;
                }
            }
            CPoint.row(i) << P2Didx.colwise().mean();
        }
        return CPoint;
    } else {
        std::cout << "Error : Max Cluster Number Error" << endl;
        cin.ignore();
        exit(-1);
    }
}

MatrixXd Belonging(const MatrixXd &Center, const MatrixXd &L, const MatrixXd &A, const MatrixXd &B, const int &ver) {
    MatrixXd Data(L.rows(), 3);
    MatrixXd DisData(L.rows(), Center.rows());
    Data << L, A, B;
    for (int i = 0; i < Center.rows(); i++) {
        if (ver == 1) {
            ArrayXXd dis, NorDis;
//			MatrixXd CenList = Center.row(i).replicate(L.rows(), 1);
//			dis = (CenList - Data).rowwise().norm();
            MatrixXd vec = (-Data).rowwise() + Center.row(i);
            dis = vec.rowwise().norm();
            NorDis = (dis - double(dis.minCoeff())) / (dis.maxCoeff() - dis.minCoeff());
            DisData.col(i) << NorDis;
        }
        if (ver == 2) {
            ArrayXXd dis, NorDis;
            MatrixXd vec = Data.rowwise() - Center.row(i);
            dis = vec.rowwise().norm();
            //cout << CenList << endl;
            NorDis = 1 / dis;
            DisData.col(i) << NorDis;
        }
        if (ver == 3) {
            ArrayXXd dis;
            MatrixXd vec = Data.rowwise() - Center.row(i);
            dis = vec.rowwise().squaredNorm();
            DisData.col(i) << dis;
        }
    }
    if (ver == 2) {
        ArrayXXd SumDis, Temp;
        SumDis = DisData.rowwise().sum().replicate(1, DisData.cols());
        //cout << DisData.row(0) << endl;
        Temp = DisData.array() / SumDis;
        DisData = Temp;
    }
    if (ver == 3) {
        ArrayXXd Ones = ArrayXXd::Ones(DisData.rows(), DisData.cols());
        ArrayXXd SumDis, Temp;
        SumDis = DisData.rowwise().sum().replicate(1, DisData.cols());
        Temp = Ones - (DisData.array() / SumDis);
//        ArrayXXd Min = Temp.rowwise().minCoeff();
//        ArrayXXd Max = Temp.rowwise().maxCoeff();
//        ArrayXXd NorDis = (Temp - Min.replicate(1, Center.rows())) / (Max-Min).replicate(1, Center.rows());
        DisData = Temp;
//        cout<<DisData<<endl;
    }
    return DisData;
}

MatrixXd
Moving(const double &alpha, const MatrixXd &Center, const MatrixXd &belong, const MatrixXd &L, const MatrixXd &A,
       const MatrixXd &B) {
    MatrixXd Data(L.rows(), 3);
    MatrixXd MoveDis = MatrixXd::Zero(L.rows(), 3);
    Data << L, A, B;
    for (int i = 0; i < Center.rows(); i++) {
        ArrayXXd dis(L.rows(), 3), TempDis, Belong(L.rows(), 3), Tempbelong;
        MatrixXd Move;
//		MatrixXd CenList = Center.row(i).replicate(L.rows(), 1);
// 		TempDis = (Data - CenList).rowwise().norm();
        MatrixXd vec = Data.rowwise() - Center.row(i);
//        TempDis = vec.rowwise().norm();

        Tempbelong = belong.array().col(i);
        Belong << Tempbelong, Tempbelong, Tempbelong;
//		dis << TempDis, TempDis, TempDis;
        Move = alpha * vec.array() * Belong;
        MoveDis = MoveDis + Move;
    }
    MatrixXd out = Data + MoveDis;
    return out;
}

Mat OutPut(const MatrixXd &Data, const Mat &original, Mat &Lab_char, MatrixXd &color) {
    Mat Rawdata, RGB, Lab;
    MatrixXd Temp(Data.rows(), 3);
    Temp.col(0) = Data.array().col(0);
    Temp.col(1) = Data.array().col(1);
    Temp.col(2) = Data.array().col(2);
//    Temp.col(0) = Data.array().col(0) * 255;
//    Temp.col(1) = Data.array().col(1) * 256;
//    Temp.col(2) = Data.array().col(2) * 256;
    eigen2cv(Temp, Rawdata);
    Lab = Rawdata.reshape(3, original.rows);
    Lab.convertTo(Lab_char, CV_8U);
    cvtColor(Lab_char, RGB, COLOR_Lab2BGR);

    vector<Mat> Lab1ch;
    MatrixXd R, G, B;
    Mat Lab3ch = RGB.reshape(0, 1);
    split(Lab3ch, Lab1ch);
    cv2eigen(Lab1ch[0].t(), B);
    cv2eigen(Lab1ch[1].t(), G);
    cv2eigen(Lab1ch[2].t(), R);
    color.resize(B.rows(), 3);
    color << B, G, R;

    //if (ColorFormat == 1){
    //	RGB.convertTo(plotting, CV_64F);
    //}
    //else
    //{
    //	Lab_char.convertTo(plotting, CV_64F);
    //}
    return RGB;
}

double Entropy(const Mat &Data) {
    double entropy = 0;
    ArrayXd Cube = ArrayXd::Zero(16581375);
    Mat Data_32f;
    Data.convertTo(Data_32f, CV_32F);
    Mat Data_array = Data_32f.reshape(1, 3).t();

    for (int i = 0; i < Data_array.rows; i++) {
        Cube(int(Data_array.at<float>(i, 0)) + 255 * max(int(Data_array.at<float>(i, 1)) - 1, 0) +
             65025 * max(int(Data_array.at<float>(i, 2)) - 1, 0)) += 1;
    }
    ArrayXd Cube_1 = Cube / 16581375;
    for (int i = 0; i < Cube_1.rows(); i++) {
        if (Cube_1(i) > 0) {
            entropy = entropy - Cube_1(i) * log2l(Cube_1(i));
        }
    }
    return entropy;
}


Mat FACE(Mat image, int apha, int ClausterNum, int GridSize, int ExplosionWay, bool opt, double Per, int Number, int iterNum, const int entropySetSize, const int entropyNumOfSets) {
    Mat Out, ReturnOut, CentMat, LAB, LineChart;
    MatrixXd L, A, B, R, P2D, Grids, Grididx, Centers, Belong, Reuslt, Color;
    Vector3d normal;

    Loadimg(image, L, A, B);
    normal = LS(L, A, B);
    R = Rotation(normal);
    P2D = Project(L, A, B, R);
    Grids = Cutgrids(P2D, Grididx, GridSize);
    Centers = ClusterCenter(Grids, Grididx, L, A, B, ClausterNum);
    Belong = Belonging(Centers, L, A, B, ExplosionWay);
    vector<double> E, percent;
//	fstream fp;
//	fp.open("/Users/bill/Desktop/Entropy/data" + to_string(Number) +  ".txt", ios::out);

    Reuslt = Moving(0, Centers, Belong, L, A, B);
    int boundertotal = (Reuslt.array() == 255).count() + (Reuslt.array() == 0).count();

    vector<double> entropySet(entropySetSize, 0.0);
    std::deque<double> entropySetAvgSet;
    std::deque<Mat> OutSet;
    int entropyIndex = 0;
    for (int i = 0; i < iterNum; i++) {

        Reuslt = Moving(double(i) / 100, Centers, Belong, L, A, B);
        Out = OutPut(Reuslt, image, LAB, Color);
        ReturnOut = Out;

        E.push_back(Entropy(Out));
        int OP = (Reuslt.array() > 255).count();
        int ON = (Reuslt.array() < 0).count();
        percent.push_back((double(OP + ON - boundertotal) / double(Reuslt.rows() * Reuslt.cols())) * 100);
        std::cout << "Iter : " << i << " " << "E : " << E[E.size() - 1] << " " << percent[percent.size() - 1] << "%" << endl;

        entropySet[entropyIndex++] = entropy_cal(Out);
        if (i % entropySetSize == 0) {
            double entropySetAvg = 0.0;
            for (double entropyVal : entropySet) {
                entropySetAvg += entropyVal;
            }
            entropySetAvg = entropySetAvg / entropySet.size();

            std::cout << "Entropy set average = " << entropySetAvg << endl;
            entropySetAvgSet.push_back(entropySetAvg);
            OutSet.push_back(Out);

            if (entropySetAvgSet.size() > entropyNumOfSets) {
                entropySetAvgSet.pop_front();
                OutSet.pop_front();
                
                bool entropyDecreasing = false;
                for (int i = entropyNumOfSets-2; i >= 0; --i) {
                    if (entropySetAvgSet[i+1] < entropySetAvgSet[i]) {
                        entropyDecreasing = true;
                    }
                    else {
                        entropyDecreasing = false;
                        break;
                    }
                }

                if (entropyDecreasing) {
                    ReturnOut = OutSet.front();
                    break;
                }
            }
            entropyIndex = 0;
        }

//		Mat dis;
//		resize(Out,dis,Size(640,480));
//		imshow("OUT",dis);
//		waitKey(10);

//        MatrixXd Temp(Reuslt.rows(), 6);
//        Temp<<Reuslt,Color;
//        fp<<Temp<<endl;

        if (percent[percent.size() - 1] >= Per) {
//			for (int j = 0; j < E.size(); j++) {
//				fp << E[j] << " " << percent[j] << endl;
//			}
//			fp.close();
            break;
        }
    }
    return ReturnOut;
}

Mat FACE_alpha(Mat image, double alpha, int ClausterNum, int GridSize, int ExplosionWay, bool opt, double Per, int Number) {
	Mat Out, CentMat, LAB, LineChart;
	MatrixXd L, A, B, R, P2D, Grids, Grididx, Centers, Belong, Reuslt, Color;
	Vector3d normal;

	Loadimg(image, L, A, B);
	normal = LS(L, A, B);
	R = Rotation(normal);
	P2D = Project(L, A, B, R);
	Grids = Cutgrids(P2D, Grididx, GridSize);
	Centers = ClusterCenter(Grids, Grididx, L, A, B, ClausterNum);
	Belong = Belonging(Centers, L, A, B, ExplosionWay);
	vector<double> E, percent;
	//	fstream fp;
	//	fp.open("/Users/bill/Desktop/Entropy/data" + to_string(Number) +  ".txt", ios::out);

	Reuslt = Moving(0, Centers, Belong, L, A, B);
	int boundertotal = (Reuslt.array() == 255).count() + (Reuslt.array() == 0).count();	

	Reuslt = Moving(alpha, Centers, Belong, L, A, B);
	Out = OutPut(Reuslt, image, LAB, Color);

	E.push_back(Entropy(Out));
	int OP = (Reuslt.array() > 255).count();
	int ON = (Reuslt.array() < 0).count();
	percent.push_back((double(OP + ON - boundertotal) / double(Reuslt.rows() * Reuslt.cols())) * 100);
    std::cout << "E : " << E[E.size() - 1] << " " << percent[percent.size() - 1] << "%" << endl;

	
	return Out;
}

double entropy_cal(Mat img)
{
	double result = 0;
	if (img.channels() == 1)
	{
		double temp[256];
		for (int i = 0; i < 256; i++)
		{
			temp[i] = 0.0;
		}
		for (int m = 0; m < img.rows; m++)
		{
			const uchar* t = img.ptr<uchar>(m);
			for (int n = 0; n < img.cols; n++)
			{
				int i = t[n];
				temp[i] = temp[i] + 1;
			}
		}
		for (int i = 0; i < 256; i++)
		{
			temp[i] = temp[i] / (img.rows*img.cols);
		}
		for (int i = 0; i < 256; i++)
		{
			if (temp[i] == 0.0)
				result = result;
			else
				result = result - (temp[i] * log2(temp[i]));
		}
	}
	else
	{
		vector<Mat> channels;
		split(img, channels);
		for (int i = 0; i < 3; i++)
		{
			Mat imgCh = channels.at(i);
			double temp[256];
			for (int i = 0; i < 256; i++)
			{
				temp[i] = 0.0;
			}
			for (int m = 0; m < imgCh.rows; m++)
			{
				const uchar* t = imgCh.ptr<uchar>(m);
				for (int n = 0; n < imgCh.cols; n++)
				{
					int i = t[n];
					temp[i] = temp[i] + 1;
				}
			}
			for (int i = 0; i < 256; i++)
			{
				temp[i] = temp[i] / (imgCh.rows*imgCh.cols);
			}
			double resultCH = 0;
			for (int i = 0; i < 256; i++)
			{
				if (temp[i] == 0.0)
					resultCH = resultCH;
				else
					resultCH = resultCH - (temp[i] * log2(temp[i]));
			}
			result += resultCH;
		}
		result = result / 3;
	}

	return result;
}