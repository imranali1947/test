#include <iostream>
#include <vector>
#include <algorithm>
#include <Eigen/Dense>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
//#include <opencv2/viz.hpp>
//#include <opencv2/viz/widgets.hpp>



cv::Mat FACE(cv::Mat image, int apha, int ClausterNum = 1, int GridSize = 3, int ExplosionWay = 2, bool opt = false, double Per = 15,int Number = 1, int iterNum = 1000, const int entropySetSize = 15, const int entropyNumOfSets = 3);
cv::Mat FACE_alpha(cv::Mat image, double alpha, int ClausterNum = 1, int GridSize = 3, int ExplosionWay = 2, bool opt = false, double Per = 15, int Number = 1);
double entropy_cal(cv::Mat img);